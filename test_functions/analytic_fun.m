function obj_fun=analytic_fun(fun_name,dim)
% INPUT : name of an analytic (closed form) function, input dimension
%
% OUTPUT : function handler (whose argument is a 1Xd row vector) of
%          maximization function
%

fh=str2func(fun_name);
if nargin(fh)==2
    % get dimXdim random rotation matrix
    R=randn(dim,dim);   [Q,~]=qr(R);    
    obj_fun = @(x)-fh(x,Q);
else
    obj_fun=@(x)-fh(x);
end
       

end