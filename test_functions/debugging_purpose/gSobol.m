function y=gSobol(x)
        %set g function of sobol in 2 dimensions
        a=[1,2];
        y=prod((abs(4*x-2)+a)./(1+a));
end



