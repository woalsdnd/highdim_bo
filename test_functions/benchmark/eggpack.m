function y=eggpack(x)

d = length(x);

%put values inside the range
for i=1:d
   if x(i)>1,x(i)=1;end
   if x(i)<-1,x(i)=-1;end
end

mult = 1;
sin_ = 0;
for ii = 1:d
	xi = x(ii);
    mult=mult*(-xi+1)/2;
    sin_=sin_+sin(7*pi/2*xi);
end

t=power(mult,1/d);

y = t*sin_/d;

end



