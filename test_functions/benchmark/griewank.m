function y=griewank(x)

d = length(x);

%rescale the domain to [-600,600]^*
bounds = [-600,600];
x(1:d) = bounds(1) + ...
    ((x(1:d)+1)/2)*(bounds(2) - bounds(1));

sum = 0;
prod = 1;

for ii = 1:d
	xi = x(ii);
	sum = sum + xi^2/4000;
	prod = prod * cos(xi/sqrt(ii));
end

y = sum - prod + 1;

end



