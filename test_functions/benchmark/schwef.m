function y=schwef(x)

d = length(x);
%put values inside the range
for i=1:d
    if x(i)>1,x(i)=1;end
    if x(i)<-1,x(i)=-1;end
end

%rescale the domain to [-500,500]^*
bounds = [-500,500];
x(1:d) = bounds(1) + ...
    ((x(1:d)+1)/2)*(bounds(2) - bounds(1));

sum = 0;
for ii = 1:d
	xi = x(ii);
	sum = sum + xi*sin(sqrt(abs(xi)));
end

y = 418.9829*d - sum;

end



