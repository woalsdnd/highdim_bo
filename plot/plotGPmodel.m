function plotGPmodel(model,next_x,plotMean,plotVar,plotUCB,plotEachUCB)
%   plot GP models 
%   INPUT : model,next point,
%           
%   FLAG : plotMean(0 or 1)
%          plotVar (0 or 1)
%          plotUCB (0 or 1)
%          plotEachUCB (0 or 1) 
%           |-> ucb on each dimension in additive model
%          

        c=model.bounds(1, 2);
        [X,Y]=meshgrid(-c:c/30:c,-c:c/30:c);
       
        if strcmp(model.kernel_type,'additive')
            [mean,var] = arrayfun(@(x,y) additive_mean_var(model,[x,y],0), X,Y); 
            ucb1=arrayfun(@(x,y) additive_acq(model,x,1), X,Y);
            ucb2=arrayfun(@(x,y) additive_acq(model,y,2), X,Y);
            ucb=ucb1+ucb2;
        elseif strcmp(model.kernel_type,'ard')
            [mean,var]=arrayfun(@(x,y) SEard_mean_var(model,[x,y]), X,Y);
            ucb=arrayfun(@(x,y) SEard_acq(model,[x y]), X,Y);
        end
        
        if plotMean
            figure; 
            pcolor(X,Y,mean); shading flat;colorbar;%caxis([0,5]);
            hold on;
            scatter(model.X(:,1),model.X(:,2),30,'k','filled'); 
            scatter(next_x(:,1),next_x(:,2),30,'w','filled'); 
            saveas(gcf,[getenv('HOME_DIR') 'image/' num2str(model.n) '_posterior_mean.eps'],'epsc2');
            close all;
            
        end
        
        if plotVar
            figure;
            pcolor(X,Y,var); shading flat;colorbar;%caxis([0,5]);
            hold on;
            scatter(model.X(:,1),model.X(:,2),30,'k','filled'); 
            scatter(next_x(:,1),next_x(:,2),30,'w','filled'); 
            saveas(gcf,[getenv('HOME_DIR') 'image/' num2str(model.n) '_var.eps'],'epsc2');
            close all;
        end
        
        if plotUCB
             if exist('plotEachUCB','var') && plotEachUCB==1 && strcmp(model.kernel_type,'additive')
                figure;
                pcolor(X,Y,ucb1); shading flat;colorbar;%caxis([0,5]);
                hold on;
                scatter(model.X(:,1),model.X(:,2),30,'k','filled'); 
                scatter(next_x(:,1),next_x(:,2),30,'w','filled'); 
                saveas(gcf,[getenv('HOME_DIR') 'image/' num2str(model.n) '_ucb1.eps'],'epsc2');
                close all;
            
                figure;
                pcolor(X,Y,ucb2); shading flat;colorbar;%caxis([0,5]);
                hold on;
                scatter(model.X(:,1),model.X(:,2),30,'k','filled'); 
                scatter(next_x(:,1),next_x(:,2),30,'w','filled'); 
                saveas(gcf,[getenv('HOME_DIR') 'image/' num2str(model.n) '_ucb2.eps'],'epsc2');
                close all;
             end
                 
            figure;
            pcolor(X,Y,ucb); shading flat;colorbar;%caxis([0,5]);
            hold on;
            scatter(model.X(:,1),model.X(:,2),30,'k','filled'); 
            scatter(next_x(:,1),next_x(:,2),30,'w','filled'); 
            saveas(gcf,[getenv('HOME_DIR') 'image/' num2str(model.n) '_ucb.eps'],'epsc2');
            close all;
                    
        end
end
