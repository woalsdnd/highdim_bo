function plotFun(model)
% INPUT : model, objective function, figure number
%
    c=model.bounds(1, 2);   n_interval=50;
    [X,Y]=meshgrid(-c:c/n_interval:c,-c:c/n_interval:c);
   
	true_fun=arrayfun(@(x,y) (model.obj_fun([x,y])), X,Y);
            
    % plot colormap
	figure;
	pcolor(X,Y,true_fun);   shading flat;   colorbar;%caxis([-50,0]);
	saveas(gcf,[getenv('HOME_DIR') 'image/objective_function.eps'],'epsc2');
    close all;
    
    % plot 3d
    figure;
	surf(X,Y,true_fun);   shading flat;   colorbar;%caxis([-50,0]);
	saveas(gcf,[getenv('HOME_DIR') 'image/objective_function_3d.eps'],'epsc2');
    close all; 
end
