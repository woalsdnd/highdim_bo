function next_point = retrieve_additive(model)
    D=model.high_dim;
    next_point=zeros(1,D);
    opt = optimset('GradObj','off','Display','off','MaxIter', 100,'Algorithm', 'sqp','TolCon', 1e-15, 'TolFun', 1e-15);
    for i=1:D
        %direct is implemented for minimization (for maximization, flip the sign) 
        problem.f=@(xi)-additive_acq(model,xi,i);
        [fmin,xmin,history]= direct(problem, model.bounds(i,:));
        optimizer=fmincon(problem.f,xmin,[],[],[],[],model.bounds(i,1),model.bounds(i,2),[],opt);
        next_point(i)=optimizer;
    end

end