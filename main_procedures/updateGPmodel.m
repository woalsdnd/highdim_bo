function updated_model=updateGPmodel(model)
% update Maximum Spanning Tree, lower cholesky matrix, maximum value, maximum point, history   
%

% update max value and maximizer
[model.max_val, argmax]= max(model.f);
model.max_x=model.X(argmax,:);
model.history_opt=[model.history_opt;[model.max_x model.max_val]];

% update hypers periodically
if mod(model.n,model.hyp_update_cycle)==0
    % learn hyper parameters 
    tic;
    model.hyp=learn_hyperparameters(model);
    fprintf('Learning Hypers : %d sec\n',toc);
end

% update lower cholesky and kernel matrices if necessary
if mod(model.n,model.hyp_update_cycle)==0 || size(model.L,1)==0
    % update lower cholesky
    if strcmp(model.kernel_type,'additive')
        cov = model.cov_model(model.hyp,model.X,model.X, 0) + exp(2*model.noise)*eye(model.n);
    elseif strcmp(model.kernel_type,'ard')
        cov = model.cov_model(model.hyp,model.X,model.X) + exp(2*model.noise)*eye(model.n);
    end
    model.L = chol(cov, 'lower');
else
    % append new row on the previous cholesky matrix
    prev_X = model.X(1:model.n-1,:);
    new_x=model.X(model.n,:);
    if strcmp(model.kernel_type,'additive')
        cross_cov = model.cov_model(model.hyp,prev_X,new_x, 0);
        auto_cov = model.cov_model(model.hyp,new_x,new_x, 0)+exp(2*model.noise);
    elseif strcmp(model.kernel_type,'ard')
        cross_cov = model.cov_model(model.hyp,prev_X,new_x);
        auto_cov = model.cov_model(model.hyp,new_x,new_x)+exp(2*model.noise);
    end
    new_column = model.L\cross_cov;
    new_corner = sqrt(auto_cov - new_column'*new_column);
    model.L = [[model.L, zeros(model.n-1, 1)];[new_column', new_corner]];
end

updated_model=model;

end
