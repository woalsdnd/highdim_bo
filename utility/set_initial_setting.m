function [obj_fun,init_pts,init_fs,bounds_domain]=set_initial_setting(method,n_init_pts,bounds_inits,high_dim,projection_dim,original_obj_fun)
% INPUT : method
%           1:original BO
%           2:random basis
%           3:random projection
%           4:subspace_learning
%           5:additive model
%
%         number of initial points
%         original dimension
%         projected(reduced) dimension
%         original objective function
%
% OUTPUT : objection function (after projection), initial points,
%          initial function values, bounds (in the domain)
%

if method==2 || method==3  || method==4    %projection based methods
    %[-sqrt(d),sqrt(d)]^d for projection based models 
    %(empirical value used in Bayesian optimization in high dimensions via random embeddings)
    bounds_domain = sqrt(projection_dim)*ones(projection_dim, 2); 
    bounds_domain(:, 1) = -bounds_domain(:, 1);
    
    if method==2    %random basis
        [A,~]=qr(randn(high_dim, projection_dim),0);
    elseif method==3 %random embedding
        A = randn(high_dim, projection_dim);
    else             %subspace learning
        n_centers=max(projection_dim,ceil(n_init_pts/(high_dim+1)));
        % evaluate function (D+1)*n_centers for derivative calculation
        % CAVEAT : n_centers >= projection_dim (if rank of X is lower than
        % k, SVD leads to meaningless basis for insufficient rank
        [A,init_pts,init_fs]=learn_subspace(n_centers,projection_dim,high_dim,original_obj_fun);
    end
    
    %initialize the objective function w.r.t projected dimension
    obj_fun = @(x) original_obj_fun((A*x')'); 
    
    %evaluate points randomly for random projection methods
    if method~=4
        [init_pts,init_fs]=random_selection(obj_fun,n_init_pts,projection_dim,bounds_inits,bounds_domain);
    end
    
else   %non-projection based methods (original BO, additive model)
    %[-1,1]^D for original BO, additive
    bounds_domain = ones(high_dim, 2); 
    bounds_domain(:, 1) = -bounds_domain(:, 1);

    obj_fun=original_obj_fun;
    [init_pts,init_fs]=random_selection(obj_fun,n_init_pts,high_dim,bounds_inits,bounds_domain);
    
end

end
