function model=set_model_parameters(model,method,hrange,n_init_pts,n_iterations,high_dim,projection_dim,bounds,hyp_update_cycle)
%   add necessary parameters based on user-specific, function-specific
%   parameters
%
%   INPUT : model (with user-specific, function-specific parameters), method
%           range of hyperparameters, #initial points, #tirals in BO loop, 
%           high_dim, projeciton_dim, bounds in the domain, cycle of updating hyperparameters
%
%   OUTPUT : model with additional fields 
%

% set #current points, #initial points, #iterations in BO loop, 
%    cycle of updating hyperparameters
model.n = length(model.f);
model.n_init_pts=n_init_pts;
model.n_iterations=n_iterations;
model.hyp_update_cycle=hyp_update_cycle;

% set original (high) and projection dimensions
model.high_dim=high_dim;
if method==1
    model.projection_dim=high_dim;
else
    model.projection_dim=projection_dim;
end

% set a kernel type corresponding to the method
if method==5
    model.kernel_type='additive';
else
    model.kernel_type='ard';
end

% set discretized domain, k^(i)(x^(i),x^(i)), k^(i)(x^(i),X^(i)),
%    k^(i)(x^(i),X^(i))K^{-1}k^(i)(x^(i),X^(i))'+2*k^(i)(x^(i),X^(i))K^{-1}1

  
% set a kernel
if strcmp(model.kernel_type,'ard')
    model.cov_model = @(hyp, x, z)covSEard(hyp, x, z);
elseif strcmp(model.kernel_type,'additive')
    model.cov_model = @(hyp, x, z, i)covAdditive(hyp, x, z, i);
end

% set domain bounds
model.bounds=bounds;

% set boundary for hyperparameters in logarithm for optimization 
if method==5
    model.hyper_bound=log([ones(model.high_dim,1)*hrange.lengthscale;hrange.signal]);
else
    model.hyper_bound=log([ones(model.projection_dim,1)*hrange.lengthscale;hrange.signal]);
end
model.noise_bound=log(hrange.noise);

% initialize hyperparameters (ell1,ell2,...,signal) in ard kernel and noise
% to the middle values in the ranges
if method==5
    model.hyp = log([ones(model.high_dim, 1)*mean(hrange.lengthscale) ; mean(hrange.signal)]);      
else
    model.hyp = log([ones(model.projection_dim, 1)*mean(hrange.lengthscale) ; mean(hrange.signal)]);      
end    
model.noise = log(mean(hrange.noise));

% set lower cholesky
model.L=[];

% set history of optimal points and chosen points
model.history_opt=[];
model.history_chosen=[];
end
